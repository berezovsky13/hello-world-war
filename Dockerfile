FROM tomcat:8.0.20-jre8

MAINTAINER Daniel-Berezovsky
COPY /1.0-SNAPSHOT/hello-world-war-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/hello-world-war.war

EXPOSE 8001
