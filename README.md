Hello World! 
===============

This is a Java servlet project.

About the Production enviorment - you need to install metric server and run the commands:
sudo microk8s kubectl apply -f deployment.yaml
sudo microk8s kubectl apply -f hpa.yaml
(or sudo microk8s kubectl apply -f /Production/)

About jenkins yaml files - you can run them all by using the command:
sudo microk8s kubectl apply -f /.jenkins/

If you want to find to password for logging Jenkins run the command:

sudo microk8s kubectl logs [pod NAME]
